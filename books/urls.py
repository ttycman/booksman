from django.urls import path, include
from rest_framework import routers

from . import views


app_name = 'books'

router = routers.DefaultRouter()
router.register('', views.VolumeApiView, 'Volume')

urlpatterns = [
    path('', views.volumes_list, name='list'),
    path('add_volume/', views.AddVolumeView.as_view(), name='add_volume'),
    path('import/', views.ImportVolumesView.as_view(), name='import'),
    path('import_action/<str:query>/', views.import_action, name='import_action'),
    path('api/', include(router.urls)),
]
