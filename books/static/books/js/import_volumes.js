function check() {
    $('#info').html('');
    $('#hidden_question').hide();

    $.ajax({
        type: 'POST',
        url: '/import/',
        data: {
            query: $('input[id=id_query]').val(),
            'csrfmiddlewaretoken': csrf_token
        },

        success: function(data) {
            if (data === 'None') {
                $('#info').html('No volumes found. Try again.');
                $('input[id=id_query]').focus();
            } else {
                $('#info').html('Found ' + data + ' volumes.');
                $('#hidden_question').show();
            }
        },

        error: function(data) {
            console.log('error: ', data);
        }
    });
}

function do_import() {
    let query = document.querySelector('input[id=id_query]');
    location.href = '/import_action/' + query.value;
}


$("#form_query").submit(function(e) {
      e.preventDefault();
      check();
 });