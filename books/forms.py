from django import forms

from .models import Volume, IndustryIdentifier, Author


class SearchForm(forms.Form):
    CHOICES = [
        ('title', 'Title'),
        ('authors', 'Authors'),
        ('language', 'Language'),
        ('pubdate', 'Published Date (year or start-end)'),
    ]
    pattern = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))
    search_by = forms.ChoiceField(choices=CHOICES, label='Search by')


class AddVolumeForm(forms.ModelForm):
    class Meta:
        model = Volume
        fields = ['title', 'publishedDate', 'pageCount', 'language']
        labels = {
            'publishedDate': 'Published Date',
            'pageCount': 'Page Count',
            'imlink_sm': 'ImageLink Small Thumbnail',
            'imlink': 'ImageLink Thumbnail',
        }
        help_texts = {
            'publishedDate': 'YYYY-MM-DD',
        }

    imlink = forms.URLField(required=False, label='ImageLink Thumbnail')
    imlink_sm = forms.URLField(required=False, label='ImageLink Small Thumbnail')


IndustryIdentifierFormset = forms.inlineformset_factory(
    Volume, IndustryIdentifier,
    fields=['type', 'identifier'],
    extra=2,
    max_num=2,
    can_delete=False,
)


AuthorFormset = forms.inlineformset_factory(
    Volume, Author,
    fields=['name'],
    extra=2,
    max_num=2,
    can_delete=False,
)


class ImportQueryForm(forms.Form):
    query = forms.CharField(max_length=300, widget=forms.TextInput(attrs={'autofocus': 'autofocus'}))
