from django.contrib import admin

from .models import Author, IndustryIdentifier, Volume


admin.site.register(Author)
admin.site.register(IndustryIdentifier)
admin.site.register(Volume)
