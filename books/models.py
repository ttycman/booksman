from django.db import models
from django_mysql.models import JSONField


class Volume(models.Model):
    title = models.CharField(max_length=255)
    publishedDate = models.DateField(null=True, blank=True)
    pageCount = models.PositiveIntegerField()
    imageLinks = JSONField()
    language = models.CharField(max_length=20)

    def __str__(self):
        return self.title


class Author(models.Model):
    name = models.CharField(max_length=255)
    volume = models.ForeignKey(Volume, on_delete=models.CASCADE, related_name='authors')

    def __str__(self):
        return self.name


class IndustryIdentifier(models.Model):
    TYPE_CHOICES = (
        ('ISBN_10', 'ISBN_10'),
        ('ISBN_13', 'ISBN_13'),
        ('ISSN', 'ISSN'),
        ('OTHER', 'OTHER'),
    )
    type = models.CharField(choices=TYPE_CHOICES, max_length=7)
    identifier = models.CharField(max_length=255)
    volume = models.ForeignKey(Volume, on_delete=models.CASCADE, related_name='industryIdentifiers')

    def __str__(self):
        return self.identifier
