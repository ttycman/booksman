from datetime import datetime as dt

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.db.models import Q
from django.views import View
from django.views.decorators.http import require_http_methods
from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from .models import Volume, Author, IndustryIdentifier
from .forms import SearchForm, AddVolumeForm, IndustryIdentifierFormset, AuthorFormset, ImportQueryForm
from .serializers import VolumeSerializer

from libs.volumes_import import get_volumes


def volumes_list(request):
    vol_count = Volume.objects.count()
    volumes = None

    if request.method == 'GET':
        form = SearchForm()

    elif request.method == 'POST':
        form = SearchForm(request.POST)

        if form.is_valid():
            cd = form.cleaned_data
            pattern = cd['pattern']
            search_by = cd['search_by']

            if search_by == 'title':
                volumes = Volume.objects.filter(title__icontains=pattern)

            elif search_by == 'authors':
                volumes = Volume.objects.filter(authors__name__icontains=pattern)

            elif search_by == 'language':
                volumes = Volume.objects.filter(language__iexact=pattern)

            elif search_by == 'pubdate':
                if '-' in pattern:
                    year_start, year_end = pattern.split('-')
                    if not year_end:
                        year_end = dt.now().year

                    date_filter = Q(publishedDate__year__gte=year_start, publishedDate__year__lte=year_end)
                else:
                    date_filter = Q(publishedDate__year=pattern)

                volumes = Volume.objects.filter(date_filter)

    context = {
        'form': form,
        'vol_count': vol_count,
        'volumes': volumes,
    }
    return render(request, 'books/books_list.html', context)


class AddVolumeView(View):
    def get(self, request):
        form = AddVolumeForm()
        formset_indident = IndustryIdentifierFormset()
        formset_author = AuthorFormset()

        context = {
            'form': form,
            'formset_indident': formset_indident,
            'formset_author': formset_author,
        }
        return render(request, 'books/add_volume.html', context)

    def post(self, request):
        form = AddVolumeForm(request.POST)

        if form.is_valid():
            volume = form.save(commit=False)
            image_links = {
                'smallThumbnail': form.cleaned_data['imlink_sm'],
                'thumbnail': form.cleaned_data['imlink'],
            }
            volume.imageLinks = image_links
            volume.save()

            if volume:
                formset_indident = IndustryIdentifierFormset(request.POST, instance=volume)
                if formset_indident.is_valid():
                    formset_indident.save()

                formset_author = AuthorFormset(request.POST, instance=volume)
                if formset_author.is_valid():
                    formset_author.save()

        return redirect('books:list')


class ImportVolumesView(View):
    def get(self, request):
        form = ImportQueryForm()
        return render(request, 'books/import.html', {'form': form})

    def post(self, request):
        query = request.POST['query']
        if query:
            num_of_volumes = get_volumes(query, check_only=True)
            return HttpResponse(num_of_volumes)

        return HttpResponse(None)


@require_http_methods(['GET'])
def import_action(request, query):
    def validate_item_data(raw_data):
        # publishedDate
        if 'publishedDate' in raw_data:
            published_date = raw_data['publishedDate']
            # if missing month and day
            if len(published_date) == 4:
                published_date = f'{published_date}-01-01'
            # if missing day
            elif len(published_date) == 7:
                published_date = f'{published_date}-01'
        else:
            # if no date
            published_date = None

        # imageLinks
        image_links = {'smallThumbnail': '', 'thumbnail': ''}
        if 'imageLinks' in raw_data:
            image_links['thumbnail'] = raw_data['imageLinks']['thumbnail']
            image_links['smallThumbnail'] = raw_data['imageLinks']['smallThumbnail']

        data = {
            'publishedDate': published_date,
            'imageLinks': image_links,
            'title': raw_data['title'].strip(),
            'language': raw_data['language'],
            'pageCount': raw_data['pageCount'] if 'pageCount' in raw_data else 0,
        }
        return data

    vol_data = get_volumes(query)

    # iterate volumes
    volumes_imported = 0
    for item in vol_data['items']:
        item_raw = item['volumeInfo']
        item_data = validate_item_data(item_raw)

        volume = Volume.objects.create(**item_data)

        if volume:
            if 'industryIdentifiers' in item_raw:
                for ident in item_raw['industryIdentifiers']:
                    IndustryIdentifier.objects.create(
                        type=ident['type'],
                        identifier=ident['identifier'],
                        volume=volume)

            if 'authors' in item_raw:
                for author_info in item_raw['authors']:
                    Author.objects.create(name=author_info, volume=volume)

            volumes_imported += 1

    context = {
        'volumes_imported': volumes_imported,
    }
    return render(request, 'books/import_action.html', context)


class VolumeApiView(viewsets.ModelViewSet):
    serializer_class = VolumeSerializer
    filter_backends = [SearchFilter]
    search_fields = ['title', 'authors__name', 'language', 'publishedDate']

    def get_queryset(self, *args, **kwargs):
        queryset_list = Volume.objects.all()

        q_title = self.request.GET.get('title')
        q_year = self.request.GET.get('year')
        q_author = self.request.GET.get('author')
        q_language = self.request.GET.get('language')

        if q_title:
            queryset_list = queryset_list.filter(title__icontains=q_title)

        if q_year:
            if '-' in q_year:
                y_start, y_end = q_year.split('-')
                if not y_end:
                    y_end = dt.now().year
                qfilter = Q(publishedDate__year__gte=y_start, publishedDate__year__lte=y_end)
            else:
                qfilter = Q(publishedDate__year=q_year)

            queryset_list = queryset_list.filter(qfilter)

        if q_author:
            queryset_list = queryset_list.filter(authors__name__icontains=q_author)

        if q_language:
            queryset_list = queryset_list.filter(language=q_language)

        return queryset_list
