from rest_framework import serializers

from .models import Volume, Author, IndustryIdentifier


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['name']

    def to_representation(self, instance):
        return instance.name


class IndustryIdentifierSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndustryIdentifier
        fields = ('type', 'identifier')


class VolumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Volume
        fields = ('id', 'title', 'authors', 'publishedDate', 'pageCount', 'imageLinks',
                  'language', 'authors', 'industryIdentifiers')

    authors = AuthorSerializer(many=True, read_only=True)
    industryIdentifiers = IndustryIdentifierSerializer(many=True, read_only=True)
