# BooksMan
Simple Books Manager


## API
Endpoint: `/api/`

Possible parameters:
`search`, `title`, `author`, `language`, `year`

`year` can be a year or range of years (start-end, start-)


Examples:

`/api/?search=python&year=2010-`

`/api/?title=django&author=reinhardt&year=2005`
