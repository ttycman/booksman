import requests


def get_volumes(query, check_only=False):
    base_url = 'https://www.googleapis.com/books/v1/volumes?q='

    url = base_url + query
    res = requests.get(url)

    if not res:
        return False

    res = res.json()

    if 'items' not in res:
        return None

    num_of_volumes = len(res['items'])
    if check_only:
        if not num_of_volumes:
            return None

        return num_of_volumes

    return res
