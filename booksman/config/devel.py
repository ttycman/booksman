from booksman.config.base import *


DEBUG = True

SECRET_KEY = 'y!3$!=jj(3ol9@(6!h4#5e4#omiyhy*soasy4igi^lrg#_d@dk'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
