from booksman.config.base import *
import os


DEBUG = False

SECRET_KEY = os.environ.get('SECRET_KEY')

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),

    },

    #     'default': {
    #         'ENGINE': 'django.db.backends.mysql',
    #         'NAME': '',
    #         'USER': '',
    #         'PASSWORD': '',
    #     }
}
